import { User } from "./user.entity"
import * as bcrypt from 'bcryptjs';

describe('User Entity', () => {
    let user: User;
    beforeEach(() => {
        user = new User();
        user.password = 'testPassword';
        user.salt = 'testSalt';
        bcrypt.hash = jest.fn();
    })

    describe('validatePassword', () => {
        it('return true as password is valid', async () => {
            bcrypt.hash.mockReturnValue('testPassword');
            expect(bcrypt.hash).not.toHaveBeenCalled();
            const result = user.validatePassword('12345678');
            expect(bcrypt.hash).toHaveBeenCalledWith('12345678', 'testSalt');
        })        

        it('return false as password is invalid', () => {
            bcrypt.hash.mockReturnValue('wrongPassowrd');
            expect(bcrypt.hash).not.toHaveBeenCalled();
            const result = user.validatePassword('wrongPassword');
            expect(bcrypt.hash).toHaveBeenCalledWith('wrongPassword', 'testSalt');
        })        
    })
        
})
