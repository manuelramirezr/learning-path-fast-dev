import { Test } from "@nestjs/testing";
import { UserRepository } from "./user.repository";
import { ConflictException, InternalServerErrorException } from "@nestjs/common";
import { User } from "./user.entity";
import * as bcrypt from 'bcryptjs'

const mockCredentialDto = {username : 'TestUsername', password:'TestPassword'};

describe('UserRepository', () => {
    let userRepository;

    beforeEach(async()=> {
        const module = await Test.createTestingModule({
            providers: [
                UserRepository,
            ]
        }).compile();

        userRepository = await module.get<UserRepository>(UserRepository);

    });

    describe('signUp', () => {
        let save;

        beforeEach(() => {
            save = jest.fn();
            userRepository.create = jest.fn().mockReturnValue({save});
        }) 

        it('successfully sign up the user', ()=> {
            save.mockResolvedValue(undefined);
            expect(userRepository.signUp(mockCredentialDto)).resolves.not.toThrow();
        });

        it('throw a conflict exception as username already exists', ()=> {
            save.mockRejectedValue({code: '23505'});
            expect(userRepository.signUp(mockCredentialDto)).rejects.toThrow(ConflictException);
        });

        it('throw a conflict exception as username already exists', ()=> {
            save.mockRejectedValue({code: '123123'}); // unhandlerd error code
            expect(userRepository.signUp(mockCredentialDto)).rejects.toThrow(InternalServerErrorException);
        });

    });

    describe('validateUserPassword', () => {
        let user;

        beforeEach(() => {
            userRepository.findOne = jest.fn();

            user = new User();
            user.username = 'TestUsername';
            user.validatePassword = jest.fn();
        })
        it('return the useranme as validation is successfull', async ()=> {
            userRepository.findOne.mockResolvedValue(user);
            user.validatePassword.mockResolvedValue(true);
            const result = await userRepository.validateUserPassword(mockCredentialDto);
            expect(result).toEqual('TestUsername')
        });
        
        it('returns null as user canot be found', async ()=> {
            userRepository.findOne.mockResolvedValue(null);
            const result = await userRepository.validateUserPassword(mockCredentialDto);
            expect(user.validatePassword).not.toHaveBeenCalled();
            expect(result).toBeNull();
        });
        
        it('return null as password is invalid', async ()=> {
            userRepository.findOne.mockResolvedValue(user);
            user.validatePassword.mockResolvedValue(false);
            const result = await userRepository.validateUserPassword(mockCredentialDto);
            expect(user.validatePassword).toHaveBeenCalled();
            expect(result).toBeNull();

        });
        
    });

    describe('hashPassword', () => {
        it('calls bcrypt.hash to generate a hash', async() => {
            bcrypt.hash = jest.fn().mockResolvedValue('testHash');
            expect(bcrypt.hash).not.toHaveBeenCalled();
            const result = await userRepository.hashPassword('testPassword', 'testSalt');
            expect(bcrypt.hash).toHaveBeenCalledWith('testPassword', 'testSalt');
            expect(result).toEqual('testHash');
        }) 
    })
    
    
    
})
