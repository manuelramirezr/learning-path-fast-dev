import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { TaskRepository } from './task.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './task.entity';
import { TaskStatus } from './task-status.enum';
import { User } from 'src/auth/user.entity';

@Injectable()
export class TasksService {

  constructor(
    @InjectRepository(TaskRepository)
    private taskRepository: TaskRepository,
  ){}

  // getAllTasks() {
  //   return this.tasks;
  // }

  // getTaskWithFilters(filterDto : GetTasksFilterDto): Task[]{
  //   const {status, search} = filterDto;
  //   let tasks = this.getAllTasks();
  //   if (status){
  //     tasks = tasks.filter(task => task.status === status);
  //   }
  //   if (search){
  //     tasks = tasks.filter(task=> 
  //       task.title.includes(search) || 
  //       task.description.includes(search), 
  //     );
  //   }
  //   return tasks;
    
  // }

  async getTaskById(id:number, user: User) : Promise<Task>{
    const found = await this.taskRepository.findOne({where: {id,userId: user.id}});
    if(!found){
      throw new NotFoundException(`Task with id "${id}" not found`);
    }
    return found;
  }

  async getTasks(
    filterDto : GetTasksFilterDto,
    user: User
    ) : Promise<Task[]>{
    const tasks = await this.taskRepository.getTasks(filterDto, user);
    return tasks;
  }
  // getTaskById(id : string): Task{
  //   const found = this.tasks.find(task => task.id === id);
  //   if(!found){
  //     throw new NotFoundException(`Task with id "${id}" not found`);
  //   }
  //   return found;
  // }

  async createTask(
    createTaskDto: CreateTaskDto,
    user:User
    ): Promise<Task>{
    return this.taskRepository.createTask(createTaskDto, user);
  }

  // Esta solución no está mal pero siempre tratamos de hacer la menor cantidad de consultas y 
  // para esta solución primero hay que consultar la BD y determinar si existe o no el registro 
  // Y luego si mandar a eliminar el registro 
  // async deleteTask(id:number) : Promise<Task>{
  //   const found = await this.taskRepository.findOne(id);
  //   if(!found){
  //     throw new NotFoundException(`Task with id "${id}" not found`);
  //   }else {
  //     await this.taskRepository.remove(found);
  //   }
  //   return found;
  
  async deleteTask(id:number, user: User) : Promise<void>{
    const resultDelete = await this.taskRepository.delete({id, userId: user.id});
    if(resultDelete.affected === 0){
      throw new NotFoundException(`Task with id "${id}" not found`);
    }    
  }
    
  // }

  async updateStatusTask(id: number, status : TaskStatus, user: User) : Promise<Task>{
    const task = await this.getTaskById(id, user);
    task.status = status;
    await task.save();
    return task;
  }

}
