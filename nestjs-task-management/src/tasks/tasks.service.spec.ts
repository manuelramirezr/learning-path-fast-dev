import { Test } from '@nestjs/testing';
import { TasksService } from './tasks.service';
import { TaskRepository } from './task.repository';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { TaskStatus } from './task-status.enum';
import { NotFoundException } from '@nestjs/common';

const mockTaskRepository = () => ({
    getTasks: jest.fn(),
    findOne: jest.fn(),
    createTask: jest.fn(),
    delete: jest.fn()
});

const mockUser = {id: 12, username : 'Test User'};

describe('TaskService', () => {

    let tasksService;
    let taskRepository;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            providers: [
                TasksService,
                {provide : TaskRepository, useFactory: mockTaskRepository},
            ],
        }).compile();

        tasksService = module.get<TasksService>(TasksService);
        taskRepository = module.get<TaskRepository>(TaskRepository);
    });

    describe('getTasks', () => {
        it('get all tasks from the repository', async ()=> {
            taskRepository.getTasks.mockResolvedValue('someValue');


            expect(taskRepository.getTasks).not.toHaveBeenCalled();
            const filltes: GetTasksFilterDto = {status: TaskStatus.IN_PROGRESS, search: 'Some search query'};
            // call tasksServices.get Task
            const result = await tasksService.getTasks(filltes, mockUser);
            //expect taskRepository.getTasks TO HAVE BENN CALLED
            expect(taskRepository.getTasks).toHaveBeenCalled();

            expect(result).toEqual('someValue')

        })    
    });

    describe('getTaskById', () =>{
        it('calls taskRepository.findOne() and succesfully retrive and return the task', async () =>{
            const mockTask = {title: 'Test Task', description: 'Test desc'};
            // Es mockResolvedValue por que es una función que tiene una promesa de devolver algo
            taskRepository.findOne.mockResolvedValue(mockTask);

            const result = await tasksService.getTaskById(1, mockUser);
            expect(result).toEqual(mockTask);

            expect(taskRepository.findOne).toHaveBeenCalledWith({
                where: {
                    id:1,
                    userId: mockUser.id
                }
            })
        });

        it('throws an error as task is not found', ()=> {
            taskRepository.findOne.mockResolvedValue(null);
            expect(tasksService.getTaskById(1, mockUser)).rejects.toThrow(NotFoundException);
        });
    });
    
    describe('createTask', () => {
        it('calls taskRespository.create() and returns the results', async () =>{
            taskRepository.createTask.mockResolvedValue('someTask');

            expect(taskRepository.createTask).not.toHaveBeenCalled();
            const task = {title: 'Title Task', description:'Test Task'};
            const result = await tasksService.createTask(task, mockUser);
            expect(taskRepository.createTask).toHaveBeenCalledWith(task, mockUser);
            expect(result).toEqual('someTask');
        });
    })
    
    describe('deleteTask', () => {
        it('calls taskRepository.delete() to delete task', async ()=>  {
            taskRepository.delete.mockResolvedValue({affected:1});
            expect(taskRepository.delete).not.toHaveBeenCalled();
            await tasksService.deleteTask(1, mockUser);
            expect(taskRepository.delete).toHaveBeenCalledWith({id: 1, userId: mockUser.id });

        });
        it('throw an error as task could not be four', ()=>  {
            taskRepository.delete.mockResolvedValue({affected:1});
            expect(tasksService.deleteTask(1, mockUser)).rejects.toThrow(NotFoundException);
        });
    });
       
    describe('updateStatusTask', () => {
        it('update task status', async ()=> {
            const save = jest.fn().mockResolvedValue(true);

            tasksService.getTaskById = jest.fn().mockResolvedValue({
                status: TaskStatus.OPEN,
                save,
            })
            expect(tasksService.getTaskById).not.toHaveBeenCalled();
            expect(save).not.toHaveBeenCalled();
            const {id,status} = {id: 1, status: TaskStatus.IN_PROGRESS};
            const result = await tasksService.updateStatusTask(id, status, mockUser);
            expect(tasksService.getTaskById).toHaveBeenCalled();
            expect(save).toBeCalled();
            expect(result.status).toEqual(TaskStatus.IN_PROGRESS);
            
        });
    })
    
        
});