import {TypeOrmModuleOptions} from '@nestjs/typeorm';
import * as config from 'config';
const dbConfig = config.get('db');
export const typeOrmConfig: TypeOrmModuleOptions = {
    type : dbConfig.type,
    host : process.env.RDS_HOSTNAME || process.env.DB_HOST || dbConfig.host,
    port : process.env.RDS_PORT || process.env.DB_PORT || dbConfig.port,
    username : process.env.RDS_USERNAME || process.env.DB_USERNAME || dbConfig.username,
    password : process.env.RDS_PASSWORD || process.env.DB_PASSWORD || dbConfig.password,
    database : process.env.RDS_DB_NAME || process.env.DB_NAME || dbConfig.database,
    entities : [__dirname + '/../**/*.entity.{ts,js}'],
    // No recomendado para producción
    synchronize : process.env.TYPEORM_SYNC || dbConfig.synchronize,
} 